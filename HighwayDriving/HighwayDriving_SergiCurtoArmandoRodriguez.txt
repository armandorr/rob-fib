"""Sample Webots controller for highway driving benchmark."""

from vehicle import Driver

# name of the available distance sensors
sensorsNames = [
    'front',
    'front right 0',
    'front right 1',
    'front right 2',
    'front left 0',
    'front left 1',
    'front left 2',
    'rear',
    'rear left',
    'rear right',
    'right',
    'left']
sensors = {}

maxSpeed = 91
driver = Driver()
driver.setSteeringAngle(0.0)  # go straight

# get and enable the distance sensors
for name in sensorsNames:
    sensors[name] = driver.getDistanceSensor('distance sensor ' + name)
    sensors[name].enable(10)

# get and enable the GPS
gps = driver.getGPS('gps')
gps.enable(10)

# get the camera
camera = driver.getCamera('camera')
# uncomment those lines to enable the camera
# camera.enable(50)
# camera.recognitionEnable(50)

while driver.step() != -1:
    left = sensors['left'].getValue()/sensors['left'].getMaxValue()
    left2 = sensors['front left 2'].getValue()/sensors['front left 2'].getMaxValue()
    left1 = sensors['front left 1'].getValue()/sensors['front left 1'].getMaxValue()
    left0 = sensors['front left 0'].getValue()/sensors['front left 0'].getMaxValue()
    front = sensors['front'].getValue()/sensors['front'].getMaxValue()
    right0 = sensors['front right 0'].getValue()/sensors['front right 0'].getMaxValue()
    right1 = sensors['front right 1'].getValue()/sensors['front right 1'].getMaxValue()
    right2 = sensors['front right 2'].getValue()/sensors['front right 2'].getMaxValue()
    right = sensors['right'].getValue()/sensors['right'].getMaxValue()

    leftTurn = (-left + 0.2)**1
    leftTurn /= 30

    rightTurn2 = (1-(left0+left1)/2)**1
    rightTurn2 /= 10

    leftTurn3 = -(1-(right2+right1+right0)/3)**1.5
    leftTurn3 /= 5

    leftTurn4 = -(right - 1)**2
    leftTurn4 /= 30

    leftT = leftTurn+leftTurn3+leftTurn4

    rightTurn = 1 - (front)
    rightTurn /= 30

    rightT = rightTurn+rightTurn2

    driver.setSteeringAngle(rightT+leftT)

    # adjust the speed according to the value returned by the front distance sensor
    frontDistance = sensors['front'].getValue()
    frontRange = sensors['front'].getMaxValue()
    speed = maxSpeed * frontDistance / frontRange
    driver.setCruisingSpeed(speed)
    # brake if we need to reduce the speed
    speedDiff = driver.getCurrentSpeed() - speed
    if speedDiff > 0:
        driver.setBrakeIntensity(min(speedDiff*1.3 / speed, 1))
    else:
        driver.setBrakeIntensity(0)

function sensor = sensorfield_time_12H(x, y, t)
    xc = 60-t*0.15; yc = 90+t*0.05;
    sensor = 200./((x-xc).^2 + (y-yc).^2 + 200);
function sensor = sensorfield_2Peaks_12H(x, y, t)
    xc  = 50;  yc  = 100;
    sensor1 = 200./((x-xc).^2 + (y-yc).^2 + 200);
    xc2 = 100; yc2 = 100;
    sensor2 = 200./((x-xc2).^2 + (y-yc2).^2 + 200);
    sensor = sensor1+sensor2;